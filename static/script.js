var ws = new WebSocket('ws://' + document.domain + ':' + location.port + '/data')

ws.onmessage = function (event) {
    const data = event.data.split('-')
    document.getElementById(data[1]).innerHTML = data[0]
};